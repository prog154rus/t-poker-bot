<?php

namespace App\Http\Controllers;

use App\Answer;
use App\MessageGenerator;
use App\MessageSender;
use App\Vote;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;

class VotingController extends Controller
{
    private $messageGenerator;
    private $messageSender;

    /**
     * Create a new controller instance.
     *
     * @param MessageGenerator $messageGenerator
     * @param MessageSender $sender
     */
    public function __construct(MessageGenerator $messageGenerator, MessageSender $sender)
    {
        $this->messageGenerator = $messageGenerator;
        $this->messageSender = $sender;
    }

    public function createNew(Request $request)
    {
        $text = $request->text;
        $channelId = $request->channel_id;
        $user = $request->user_name;

        $newVote = new Vote();
        $newVote->channel = $channelId;
        $newVote->title = $text;
        $newVote->save();

        $voteId = $newVote->id;

        $message = $this->messageGenerator->generateMessage($voteId);


        $createdPollMessage = [
            "text" => $user . ' начал голосование по ' . $text,
            "channel" => $channelId
        ];

        $this->messageSender->send($createdPollMessage);
        $this->messageSender->send($message);

        return response('', 200);
    }

    public function action(Request $request)
    {
        $requestJson = json_decode($request->payload);
        if ($requestJson->actions[0]->name === "vote") {
            $user = $requestJson->user->name;
            $voteId = $requestJson->callback_id;
            $storyPoint = $requestJson->actions[0]->value;

            /** @var Collection $answers */
            $answers = Answer::where('user', $user)->where('vote_id', $voteId)->get();

            if ($answers->count() > 0) {
                 $answer = $answers[0];
                 $answer->story_points = $storyPoint;
                 $answer->save();
            }
            else {
                $answer = new Answer();
                $answer->user = $user;
                $answer->story_points = $storyPoint;
                $answer->vote_id = $voteId;
                $answer->save();
            }

            $message = $this->messageGenerator->generateMessage($voteId);
            return response()->json($message);
        }
        elseif ($requestJson->actions[0]->name === "vote-manage") {
            if ($requestJson->actions[0]->value === "finish") {
                $voteId = $requestJson->callback_id;
                $message = $this->messageGenerator->generateResult($voteId);
                return response()->json($message);
            }
        }
    }
}
