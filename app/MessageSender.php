<?php
/**
 * Created by PhpStorm.
 * User: m.k.khivintsev
 * Date: 06.09.2018
 * Time: 12:00
 */

namespace App;


use GuzzleHttp\Client;

class MessageSender
{
    private $client;
    private $token;

    public function __construct()
    {
        $this->client = new Client();
        $this->token = config('slack.token');
    }

    public function send($message)
    {
        $sendUrl = 'https://slack.com/api/chat.postMessage';

        $response = $this->client->post($sendUrl, [
            "json" => $message,
            "headers" => [
                "Authorization" => "Bearer " . $this->token
            ]
        ]);

        return $response;
    }
}