<?php
/**
 * Created by PhpStorm.
 * User: m.k.khivintsev
 * Date: 04.09.2018
 * Time: 16:50
 */

namespace App;


class MessageGenerator
{
    public function generateMessage($voteId)
    {
        $vote = Vote::with('answers')->find($voteId);

        $answers = $vote->answers()->get();
        $userLogins = $answers->map(function ($answer) {
            return $answer->user;
        })->implode(", ");

        $responseObject = [
            "text" => $vote->title,
            "channel" => $vote->channel,
            "token" => "8XHeORLk0Ol61mcH4yzrBIvA",
            "attachments" => [
                [
                    "fallback" => "EROR",
                    "callback_id" => $voteId,
                    "color" => "#FFDD2D",
                    "attachment_type" => "default",
                    "text" => $answers->count() == 0 ? "Никто не проголосовал :pepe:" :
                        "Проголосовали (" . $answers->count() . "): " . $userLogins
                ],
                [
                    "fallback" => "EROR",
                    "callback_id" => $voteId,
                    "color" => "#FFDD2D",
                    "attachment_type" => "default",
                    "actions" => [
                        [
                            "name" => "vote",
                            "text" => "1/2",
                            "type" => "button",
                            "value" => 0.5
                        ],
                        [
                            "name" => "vote",
                            "text" => "1",
                            "type" => "button",
                            "value" => 1
                        ],
                        [
                            "name" => "vote",
                            "text" => "2",
                            "type" => "button",
                            "value" => 2
                        ],
                        [
                            "name" => "vote",
                            "text" => "3",
                            "type" => "button",
                            "value" => 3
                        ],
                        [
                            "name" => "vote",
                            "text" => "5",
                            "type" => "button",
                            "value" => 5
                        ]
                    ]
                ],
                [
                    "fallback" => "EROR",
                    "callback_id" => $voteId,
                    "color" => "#FFDD2D",
                    "attachment_type" => "default",
                    "actions" => [
                        [
                            "name" => "vote",
                            "text" => "8",
                            "type" => "button",
                            "value" => 8
                        ],
                        [
                            "name" => "vote",
                            "text" => "13",
                            "type" => "button",
                            "value" => 13
                        ],
                        [
                            "name" => "vote",
                            "text" => "Кофе-брейк",
                            "type" => "button",
                            "value" => -1
                        ],
                        [
                            "name" => "vote",
                            "text" => "?",
                            "type" => "button",
                            "value" => -1
                        ]
                    ]
                ],
                [
                    "fallback" => "EROR",
                    "callback_id" => $voteId,
                    "color" => "#FFDD2D",
                    "attachment_type" => "default",
                    "actions" => [
                        [
                            "name" => "vote-manage",
                            "text" => "Завершить",
                            "type" => "button",
                            "value" => "finish"
                        ]
                    ]
                ]
            ]
        ];
        return $responseObject;
    }

    public function generateResult($voteId)
    {
        $vote = Vote::with('answers')->find($voteId);
        $answers = $vote->answers()->get();

        $answersWithoutCoffee = $answers->filter(function ($answer) {
            return $answer->story_points != -1;
        });

        if ($answersWithoutCoffee->count() === 0) {
            return [
                "text" => $vote->title,
                "attachments" => [
                    [
                        "fallback" => "EROR",
                        "callback_id" => $voteId,
                        "color" => "#FFDD2D",
                        "attachment_type" => "default",
                        "text" => "Перерыв на кофе!"
                    ]
                ]
            ];
        } else {
            $average = $answersWithoutCoffee->avg(function ($item) {
                return $item->story_points;
            });
            $sortedVotes = $answersWithoutCoffee->sortBy('story_points');

            $result = [
                "text" => $vote->title,
                "attachments" => [
                    [
                        "fallback" => "EROR",
                        "callback_id" => $voteId,
                        "color" => "#FFDD2D",
                        "attachment_type" => "default",
                        "text" => "Оценка " . $average
                    ]
                ]
            ];
            if ($sortedVotes->count() === 1) {
                $minimum = $sortedVotes->first();
                $result["attachments"][] = [
                    "fallback" => "EROR",
                    "callback_id" => $voteId,
                    "color" => "#FFDD2D",
                    "attachment_type" => "default",
                    "text" => $minimum->user . " поставил " . $minimum->story_points
                ];
            } else {
                $minimum = $sortedVotes->first();
                $maximum = $sortedVotes->last();
                $result["attachments"][] = [
                    "fallback" => "EROR",
                    "callback_id" => $voteId,
                    "color" => "#FFDD2D",
                    "attachment_type" => "default",
                    "text" => $minimum->user . " поставил " . $minimum->story_points
                ];
                $result["attachments"][] = [
                    "fallback" => "EROR",
                    "callback_id" => $voteId,
                    "color" => "#FFDD2D",
                    "attachment_type" => "default",
                    "text" => $maximum->user . " поставил " . $maximum->story_points
                ];
            }

            return $result;
        }
    }
}