<?php

namespace App\Providers;

use App\MessageSender;
use Illuminate\Support\ServiceProvider;

class MessageSenderProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
